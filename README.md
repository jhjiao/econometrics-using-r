# README #

* The .R files are code I wrote for my maro-economics course where R was used to analyse business cycles, GDP growth, poverty, government finance, corporate finance, as well as unemployment.
* The .pdf files are report written to illustrate the results with graphs and data output from the R code.
* I recommend running the code in R-Studio to see the graphs and plots.